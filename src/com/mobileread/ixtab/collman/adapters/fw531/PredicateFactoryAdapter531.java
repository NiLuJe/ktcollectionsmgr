package com.mobileread.ixtab.collman.adapters.fw531;

import com.amazon.kindle.content.catalog.b;
import com.amazon.kindle.content.catalog.g;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter531 implements PredicateFactoryAdapter {
	
	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private g[] explode(
			Predicate[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (g) in[i].delegate;
		}
		return out;
	}

	public Predicate startsWith(String key, String value) {
		return wrap(b.hbB(key, value));
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(b.ydB(explode(predicates)));
	}

	public Predicate isTrue(String what) {
		return wrap(b.SdB(what));
	}

	public Predicate not(Predicate pred) {
		return wrap(b.QdB((g) pred.delegate));
	}

	public Predicate equals(String key, String value) {
		return wrap(b.lvA(key, value));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(b.NCB(explode(predicates)));
	}

	public Predicate notNull(String key) {
		return wrap(b.VCB(key));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(b.VbB(key, CatalogAdapter531.asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(b.uAB(key, value, inclusive));
	}

}
