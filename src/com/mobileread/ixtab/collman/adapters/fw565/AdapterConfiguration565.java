package com.mobileread.ixtab.collman.adapters.fw565;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration565 extends AdapterConfiguration {

	public AdapterConfiguration565() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter565();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter565();
	}

	public CatalogService getCatalogService() {
		return new CatalogService565();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager565(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler565();
	}

}
