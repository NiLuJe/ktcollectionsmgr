package com.mobileread.ixtab.collman.adapters.fw565;

import com.amazon.ebook.util.a.B;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction565 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction565(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.gN((B) uuid);
	}

	public boolean commitSync() {
		return delegate.bO().ZhC();
	}

	public void addEntry(MutableEntry c) {
		delegate.gp(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.GN(entry);
	}

}
