package com.mobileread.ixtab.collman.adapters.fw545;

import java.util.Date;

import com.amazon.ebook.util.a.h;
import com.amazon.ebook.util.text.b;
import com.amazon.kindle.content.catalog.H;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter545 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		h[] ids = asUUIDArray(uuids);
		mutable.fl(ids);
	}

	protected static h[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		h[] out = new h[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (h) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.Ym((h) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		setTitle((MutableEntry)mutable, title);
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.bI();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.wG();
	}

	public String getTitle(CatalogEntry entry) {
		b[] titles = entry.jh();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.SH();
	}

	public String getCDEType(CatalogItem item) {
		return item.GI();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.Ph();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Qj();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.Jl(visible);
	}

	public boolean hasCloudCollections() {
		return true;
	}

	public int countParents(CatalogEntry entry) {
		return getParents(entry).length;
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.eJ();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		H[] credits = entry.nI();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.ul();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.YJ();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.Uk(new b[] {new b(title)});
	}

}
