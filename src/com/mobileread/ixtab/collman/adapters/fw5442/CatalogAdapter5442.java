package com.mobileread.ixtab.collman.adapters.fw5442;

import java.util.Date;

import com.amazon.ebook.util.a.h;
import com.amazon.ebook.util.text.b;
import com.amazon.kindle.content.catalog.H;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter5442 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		h[] ids = asUUIDArray(uuids);
		mutable.zk(ids);
	}

	protected static h[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		h[] out = new h[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (h) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.AL((h) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		setTitle((MutableEntry)mutable, title);
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.hH();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.Uh();
	}

	public String getTitle(CatalogEntry entry) {
		b[] titles = entry.fh();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.DJ();
	}

	public String getCDEType(CatalogItem item) {
		return item.Dj();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.ZH();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Ph();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.EL(visible);
	}

	public boolean hasCloudCollections() {
		return true;
	}

	public int countParents(CatalogEntry entry) {
		return getParents(entry).length;
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.hh();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		H[] credits = entry.eJ();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.kJ();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.Nk();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.hl(new b[] {new b(title)});
	}

}
