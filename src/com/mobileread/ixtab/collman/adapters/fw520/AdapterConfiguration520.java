package com.mobileread.ixtab.collman.adapters.fw520;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration520 extends AdapterConfiguration {

	public AdapterConfiguration520() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter520();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter520();
	}

	public CatalogService getCatalogService() {
		return new CatalogService520();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager520(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler520();
	}

}
