package com.mobileread.ixtab.collman.adapters.fw520;

import com.amazon.ebook.util.a.g;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction520 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction520(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.eg((g) uuid);
	}

	public boolean commitSync() {
		return delegate.FH().icB();
	}

	public void addEntry(MutableEntry c) {
		delegate.If(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.Oi(entry);
	}

}
