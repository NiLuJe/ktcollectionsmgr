package com.mobileread.ixtab.collman.adapters.fw540;

import java.util.Date;

import com.amazon.ebook.util.a.i;
import com.amazon.ebook.util.text.I;
import com.amazon.kindle.content.catalog.D;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter540 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		i[] ids = asUUIDArray(uuids);
		mutable.Yi(ids); // line 17
	}

	protected static i[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		i[] out = new i[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (i) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.Yj((i) uuid); //19
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		mutable.Mj(new I[] {new I(title)}); //26
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.tE(); // 19
	}

	public String getLocation(CatalogEntry entry) {
		return entry.af(); // 45
	}

	public String getTitle(CatalogEntry entry) {
		I[] titles = entry.Le(); // 51
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.Qg(); // 20
	}

	public String getCDEType(CatalogItem item) {
		return item.sg(); // 22
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.lF(); // 43
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.uE(); // 39
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.oi(visible); // 44
	}

	public int countParents(CatalogEntry entry) {
		return entry.DE(); // 27
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.Cf(); // 25
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		D[] credits = entry.uF(); //21
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.pi(); // 17
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.Ah(); // 19
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.Mj(new I[] {new I(title)}); // 26
	}

}
