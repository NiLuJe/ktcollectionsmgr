package com.mobileread.ixtab.collman.ui;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

public class Borders {

	public static final Border GRAY2 = BorderFactory.createLineBorder(Color.DARK_GRAY, 2);
	public static final Border WHITE5 = BorderFactory.createLineBorder(Color.WHITE, 5);
	public static final Border GRAY2WHITE5 = BorderFactory.createCompoundBorder(GRAY2, WHITE5);

}
