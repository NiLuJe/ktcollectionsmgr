package com.mobileread.ixtab.collman.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.ibm.icu.text.MessageFormat;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.Mouse;
import com.mobileread.ixtab.collman.Mouse.State;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;

public class EntryPanel extends JPanel implements ItemListener, ActionListener,
		Mouse.Sensitive {
	private static final long serialVersionUID = 1L;

	private static Font collectionFont = null;
	// private static Font countFont = null;
	private static Font entryFont = null;
	private static Font moreInfoFont = null;

	private final JLabel name = new JLabel();
	private final JLabel moreInfo = new JLabel();
	private final CountLabel count = new CountLabel();
	private final CountAlignDummyLabel countAlignDummy = new CountAlignDummyLabel();
	private final JCheckBox checkbox = new JCheckBox();
	private final int position;
	private Entry entry;

	private static final String noAuthorText = I18n.get().i18n(
			I18n.AUTHOR_NONE_KEY, I18n.AUTHOR_NONE_VALUE);
	private static final String collectionEmptyText = I18n.get().i18n(
			I18n.COLLECTIONCOUNT_ZERO_KEY, I18n.COLLECTIONCOUNT_ZERO_VALUE);
	private static final MessageFormat collectionSizeFormat = I18n.get()
			.instantiateFormat(I18n.COLLECTION_NONZERO_FORMAT_KEY,
					I18n.COLLECTION_NONZERO_FORMAT_VALUE);

	// that's exactly between Color.GRAY and Color.DARK_GRAY. The former
	// is just a bit too light on the PW, while the latter is just a bit
	// too dark on the Touch.
	private static final Color COLOR_HIDDEN = new Color(96, 96, 96);
	private static final Color COLOR_NORMAL = Color.BLACK;
	private static final Color COLOR_INVISIBLE = Color.WHITE;

	private static String getCollectionSizeAsText(int size) {
		if (size == 0) {
			return collectionEmptyText;
		}
		return collectionSizeFormat.format(new Object[] { new Integer(size) });
	}

	public EntryPanel(final int position) {
		prepareFonts();
		Event.addListener(this);
		this.position = position;
		setLayout(new BorderLayout(5, 1));
		// setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

		// This is a hack to ensure that checkboxes are somewhat aligned with
		// the "toggle all" box, and (roughly) centered with the description.
		checkbox.setBorder(BorderFactory.createMatteBorder(0, 7, 6, 0,
				COLOR_NORMAL));
		add(checkbox, BorderLayout.WEST);

		JPanel nameAndAuthor = new JPanel(new BorderLayout(0, 1));
		nameAndAuthor.add(name, BorderLayout.CENTER);
		nameAndAuthor.add(moreInfo, BorderLayout.SOUTH);
		add(nameAndAuthor, BorderLayout.CENTER);

		JPanel countAndDummy = new JPanel(new BorderLayout(0, 1));
		countAndDummy.add(count, BorderLayout.CENTER);
		countAndDummy.add(countAlignDummy, BorderLayout.SOUTH);

		add(countAndDummy, BorderLayout.EAST);

		checkbox.addItemListener(this);
	}

	private void prepareFonts() {
		if (collectionFont == null) {
			collectionFont = new Font(count.getFont().getName(), Font.PLAIN,
					Math.round(count.getFont().getSize() * 1.0f));
			entryFont = new Font("Serif", Font.PLAIN, Math.round(count
					.getFont().getSize() * 1.0f));
			moreInfoFont = new Font(count.getFont().getName(), Font.BOLD, Math
					.round(count.getFont().getSize() * 0.7f));
		}
	}

	private void hideLabel(JLabel label) {
		/*
		 * this doesn't actually hide a label (which would influence the layout
		 * and shift things around on the screen). Instead, it simply paints
		 * some dummy text, white on white.
		 */
		label.setForeground(COLOR_INVISIBLE);
		label.setText("( X )"); // work for all labels, but is
		// specifically formatted so for the count label
	}

	private void setCheckboxVisible(boolean visible) {
		/*
		 * checkbox.setVisible() would change the UI layout (because the
		 * checkbox gets removed entirely from the UI), resulting in "jumpy" UI
		 * behavior. Therefore, we don't actually make it invisible, but white
		 * on white, and have our own logic to prevent the checkbox from doing
		 * anything if it's invisible.
		 */
		checkbox.setForeground(visible ? COLOR_NORMAL : COLOR_INVISIBLE);
	}

	void setEntry(Entry entry) {
		checkbox.setSelected(false);

		// this is actually only needed once, but... I'm just lazy ;-p
		moreInfo.setFont(moreInfoFont);
		countAlignDummy.setFont(moreInfoFont);
		hideLabel(countAlignDummy);

		if (entry == null) {
			setCheckboxVisible(false);
			hideLabel(count);
			hideLabel(name);
			hideLabel(moreInfo);
		} else {
			setCheckboxVisible(true);
			Color color = entry.isVisible() ? COLOR_NORMAL : COLOR_HIDDEN;
			name.setForeground(color);
			count.setForeground(color);
			moreInfo.setForeground(color);

			count.setText("( " + entry.countParents() + " ) ");
			name.setText(entry.getName());

			if (entry instanceof Collection) {
				name.setFont(collectionFont);
				int size = ((Collection) entry).getSize();
				String text = getCollectionSizeAsText(size);
				moreInfo.setText(text);
			} else {
				name.setFont(entryFont);
				String author = entry.getAuthor();
				if (author == null || author.trim().length() == 0) {
					moreInfo.setText(noAuthorText);
					moreInfo.setForeground(COLOR_HIDDEN);
				} else {
					moreInfo.setText(author);
				}
			}
		}
		this.entry = entry;
	}

	public void itemStateChanged(ItemEvent e) {
		int type = e.getStateChange() == ItemEvent.SELECTED ? Event.VIEW_ENTRY_SELECTED
				: Event.VIEW_ENTRY_UNSELECTED;
		if (entry != null) {
			Event.post(type, entry, position);
		}
	}

	public void actionPerformed(final ActionEvent e) {
		if (e.getID() == Event.TOGGLE_ALL_SELECTIONS
				&& ((Event) e).appliesTo(position)) {
			if (entry != null) {
				checkbox.doClick();
			}
			return;
		}

		if (e.getID() == Event.ENTRY_CHANGED
				&& Entry.equals(e.getSource(), entry)) {
			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					setEntry((Entry) e.getSource());
				}
			});
			Event.post(Event.VIEW_CHANGED, e.getSource(), position);
		}
	}

	public void doClick() {
		if (entry != null && entry instanceof Collection) {
			Event.post(Event.VIEW_ENTRY_CLICKED, entry, position);
		}
	}

	private void showParents() {
		if (entry != null && entry.countParents() > 0) {
			Collection[] parents = entry.getParents();
			if (parents.length > 0) {
				StringBuffer s = new StringBuffer();
				for (int i = 0; i < parents.length; ++i) {
					// s.append(parents[i].toString());
					s.append(parents[i].getName());
					s.append("\n");
				}
				KOptionPane.showMessageDialog(this, s.toString(), entry
						.getName());
			}
		}
	}

	public boolean handleMouseReleased(State state) {
		if (state.isClick()) {
			if (state.isLongClick()) {
				showParents();
			} else {
				Object src = state.released.getSource();
				if (src != null && src instanceof Container) {
					Container srcComponent = (Container) src;
					Component target = srcComponent.findComponentAt(state.released.getPoint());
					if (target != null && target instanceof CountLabel) {
						showParents();
						return true;
					}
				}
				doClick();
			}
			return true;
		}
		return false;
	}
	
	private class CountLabel extends JLabel {
		private static final long serialVersionUID = 1L;
		public CountLabel() {
			super("(X) ");
		}
	}
	
	private class CountAlignDummyLabel extends CountLabel {
		private static final long serialVersionUID = 1L;
	}

}
