package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.icons.Icons;

public class ToggleLowerPanelAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private boolean enabled = true;
	private JButton button;
	private static final ImageIcon hideIcon = Icons.load(Icons.HIDE);
	private static final ImageIcon showIcon = Icons.load(Icons.SHOW);
	
	public ToggleLowerPanelAction() {
		Event.addListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		// we are not the only one to post these events
		if (e instanceof Event) {
			if (e.getID() == Event.LOWER_PANEL_DISABLE) {
				enabled = false;
				button.setIcon(showIcon);
			} else if (e.getID() == Event.LOWER_PANEL_ENABLE) {
				enabled = true;
				button.setIcon(hideIcon);
			}
		} else {
			enabled = !enabled;
			if (enabled) {
				Event.post(Event.LOWER_PANEL_ENABLE, this, PanelPosition.BOTH);
			} else {
				Event.post(Event.LOWER_PANEL_DISABLE, this, PanelPosition.BOTH);
			}
		}
	}

	public void setButton(JButton button) {
		button.setText("");
		this.button = button;
		button.setIcon(hideIcon);
	}
}
