1. Install Eclipse
2. Import the existing project into Eclipse
3. Modify build.properties. You will need to adapt the paths for the Java 1.4 SDK,
   and for *all* supported firmware libraries. You'll need to extract the libraries
   from a Kindle device running the respective version (or from an image or update
   file). It's enough to copy *all* .jar files found in the /opt/amazon/ebook/lib
   directory, and its subdirectories.
4. Run the "dist" target of build.xml to compile and prepare for packaging.

Good luck!

PS: Don't worry if part of the project's sources is displayed as "broken" in Eclipse.
    That's because different classes use different versions of the libraries, so it's
    generally not possible to satisfy all dependencies at the same time.
    The build.xml works around this by compiling in multiple stages, using the
    correct required libraries at each stage.

=================== How to extract the libraries ==========================

1. [Optional] Download the corresponding update .bin file from amazon
2. [Optional] Download KindleTool - (see http://www.mobileread.com/forums/showthread.php?t=187880)
3. [Optional] Extract corresponding .bin file - "kindletool extract update_kindle_5.4.3.2.bin"
4. [Optional] OR download an image of the rootfs - ( see http://ixtab.tk/kindle-touch-images.php)
5. Mount the corresponding image file (after gunzipping it if needed) - "mount -o ro,loop rootfs.img /mnt/test"
6. Copy /opt/amazon/ebook/lib from /mnt/test - "cp -av /mnt/test/opt/amazon/ebook/lib ./fw-5432"
7. Done

=================== Update packages & rootfs dumps ========================

For update packages, start from the Kindle Software Update page:
https://www.amazon.com/gp/help/customer/display.html?nodeId=200529680
Then choose your device, and simply tweak the URL of the download link, everything should still be stored
on Amazon's servers.

Note that, while most post 5.3.x updates are full updates with a rootfs image (the exceptions being 5.3.2 & 5.3.2.1),
older ones are just a bunch of bsdiff patches.
If you're feeling adventurous, you *can* manually apply the relevant part of those patches :).


You're also welcome to take a look at http://ixtab.tk/kindle-touch-images.php to fill in the blanks ;).
